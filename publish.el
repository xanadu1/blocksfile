;; Publish org-mode files on GitLab pages

;; This is a customized script for the Emacs editor that compiles into HTML my
;; notes in the org-mode (.org files) markup.  It is invoked by a CI pipeline
;; process, defined in the file .gitlab-ci.yml, each time I commit new content
;; to the main Git branch, to regenerate my website from my notes, making it
;; available at https://xanadu.thoughtamps.info.

(require 'org)
(require 'ox-publish)

(setq user-full-name "Xanalogica")
(setq user-mail-address "xanalogica@gmail.com")

(setq
  org-html-divs
    '(
      (preamble  "header"  "top")
      (content   "main"    "content")
      (postamble "footer"  "postamble")
    )
  org-html-container-element "section"
  org-html-metadata-timestamp-format "%Y-%m-%d"
  org-html-checkbox-type 'html
  org-html-html5-fancy t
  org-html-doctype "html5"
)

(defvar site-attachments (regexp-opt '("jpg" "jpeg" "gif" "png" "svg"
                                       "ico" "cur" "css" "js" "woff" "html" "pdf")))

(setq org-publish-project-alist
   (list
      ;; Publish as HTML all the Org-mode files into Publishing Directory
      (list "xanadu-papers-local"
         :base-directory "."
         :publishing-directory "./public"

         :base-extension "org"
         :exclude (regexp-opt '("README" "draft" "readtheorg.org"))
         :exclude "org/"

         :recursive t

         :publishing-function '(org-html-publish-to-html)

         :auto-sitemap nil
         ;;; :sitemap-filename "index.org"
         :sitemap-file-entry-format "%d *%t*"
         :sitemap-style 'list
         :sitemap-sort-files 'anti-chronologically

         :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"/favicon.ico\"/>"
      )

      ;; Copy Diagrams (and Folders) into Publishing Directory
      (list "xanadu-statics-local"
         :base-directory "."
         :publishing-directory "./public"

         :base-extension site-attachments
         :recursive t

         :exclude "public/"
         :publishing-function 'org-publish-attachment
         )
      ;; Publish Component
      (list "xanadu-local"
            :components '("xanadu-papers-local" "xanadu-statics-local")
         )
      )
   )

;; (defun publish-whitepapers ()
;;   (let
;;     (
;;       (org-export-with-section-numbers  nil)
;;       (org-export-with-smart-quotes     t)
;;       (org-export-with-toc              nil)
;;     )
;;     (org-publish-all)
;;   )
;; )

;; (defun publish-slides ()
;;   (let
;;     (
;;       (org-export-with-section-numbers  nil)
;;       (org-export-with-smart-quotes     t)
;;       (org-export-with-toc              nil)
;;     )
;;     (org-publish-all)
;;   )
;; )
